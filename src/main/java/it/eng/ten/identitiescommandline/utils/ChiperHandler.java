package it.eng.ten.identitiescommandline.utils;

import it.eng.ten.identitiescommandline.config.ApplicationConfig;
import it.eng.ten.identitiescommandline.model.ECDSApublicKeyXY;
import it.eng.ten.identitiescommandline.model.FileWallet;
import it.eng.ten.identitiescommandline.model.KeyPair;
import it.eng.ten.identitiescommandline.utils.crypto.ChiperUtils;
import it.eng.ten.identitiescommandline.utils.crypto.ECDSA;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class ChiperHandler {

    public static final String LOG_FILE_TYPE = ".txt";
    private static final Logger logger = LogManager.getLogger(ChiperHandler.class);

    public static String createGUID() {
        Random random = ThreadLocalRandom.current();
        byte[] r = new byte[64];
        random.nextBytes(r);
        return Base64.getEncoder().encodeToString(r).replace("/", "");
    }

    private static int getKeyLength(final PublicKey pk) {
        int len = -1;
        final ECPublicKey ecpriv = (ECPublicKey) pk;
        final java.security.spec.ECParameterSpec spec = ecpriv.getParams();
        if (spec != null) {
            len = spec.getOrder().bitLength(); // does this really return something we expect?
        } else {
            // We support the key, but we don't know the key length
            len = 0;
        }
        return len;
    }


    public static void createWallet(String psw, String name) throws Exception {

        final Pair<PrivateKey, PublicKey> privateKeyPublicKeyPair = ECDSA.generateKey();
        Base64.Encoder encoder = Base64.getEncoder();

        String publicKeyStr = encoder.encodeToString(privateKeyPublicKeyPair.getRight().getEncoded());
        String privateKeyStr = encoder.encodeToString(privateKeyPublicKeyPair.getKey().getEncoded());
        ECDSApublicKeyXY ecdsApublicKeyXY;

        ecdsApublicKeyXY = ECDSA.getPublicKeyAsHex(privateKeyPublicKeyPair.getRight());
        ECDSA.getPrivateKeyAsHex(privateKeyPublicKeyPair.getKey());
        final String pkeyJson = JsonHandler.convertToJson(ecdsApublicKeyXY);
        final String addr = ChiperUtils.calcolateAddr(pkeyJson);
        Path path = Paths.get(ApplicationConfig.WALLET_PATH + File.separator + "identities-wallet");

        if (Files.exists(path) && !name.equals(ApplicationConfig.INIT_NAME)) {
            String walletString =
                    new String(Files.readAllBytes(Paths.get(ApplicationConfig.WALLET_PATH + File.separator +
                            "identities-wallet")));
            String decrypt;
            try {

                decrypt = ChiperUtils.decrypt(psw, walletString);
            } catch (Exception e) {
                //logger.error("Wrong password! Try again.");
                throw new Exception("The chosen password is not valid, please retry   ");
            }
            //logger.info("Wallet exists! Add new Identity...");
            Collection<FileWallet> fileWallets = (Collection<FileWallet>) JsonHandler.convertFromJson(decrypt,
                    FileWallet.class, true);
            FileWallet fileWallet1 = new FileWallet(addr, new KeyPair(publicKeyStr, privateKeyStr, "ECDSA", name));
            fileWallets.add(fileWallet1);
            final String json = JsonHandler.convertToJson(fileWallets);
            final String encrypt = ChiperUtils.encrypt(psw, json);
            System.out.println("Address created : " + addr);
            System.out.println("PubKey created : " + publicKeyStr);
            System.out.println("Name : " + fileWallet1.getkeyPair().getName());
            writeFileCrypted(encrypt, addr);
            createLogFile(name, publicKeyStr, addr);
        } else { // NEW WALLET
            final String modelFile = createModelFile(addr, publicKeyStr, privateKeyStr, "ECDSA", name);
            final String encrypt = ChiperUtils.encrypt(psw, modelFile);
           /* logger.info("Address created : " + addr);
            logger.info("Publickey created : " + publicKeyStr);
            logger.info("Name : " + name);*/
            writeFileCrypted(encrypt, addr);
            createLogFile(name, publicKeyStr, addr);
        }

    }

    private static void createLogFile(String name, String publicKeyStr, String addr) throws IOException {
        if (name.equals(ApplicationConfig.INIT_NAME)) return;
        String pattern = "dd-MM-yyyy_HH-mm-ss-SSS";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        String logFileName = "tenid" + "_" + date;
        Path currentDir = Paths.get(System.getProperty("user.dir") + File.separator + logFileName + LOG_FILE_TYPE);
        Files.createFile(currentDir);
        StringBuilder sb = new StringBuilder("");
//        sb.append("Name: " + name);
//        sb.append("\n");
        sb.append("Address: " + addr);
        sb.append("\n");
        sb.append("PubKey: " + publicKeyStr);
        Files.write(currentDir, sb.toString().getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE);
        System.out.println("The identity's manifest has been exported to " + currentDir);

    }

    public static boolean existsIdentityFile() {
        Path path = Paths.get(ApplicationConfig.WALLET_PATH + File.separator + "identities-wallet");
        boolean existsRes = Files.exists(path);
        boolean isDirectory = Files.isDirectory(path);
        if (existsRes && isDirectory) {
            try {
                Files.delete(path);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return false;
        }
        if (existsRes) return true;
        return false;
    }


    public static String createModelFile(String address, String publicKey, String privateKey, String ecdsa,
                                         String name) throws Exception {

        List<FileWallet> fileWalletList = new ArrayList<>();
        FileWallet fileWallet = new FileWallet(address, new KeyPair(publicKey, privateKey, ecdsa, name));
        fileWalletList.add(fileWallet);
        return JsonHandler.convertToJson(fileWalletList);

    }


    private static Key stringToSecretKey(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {

        byte[] salt = {
                (byte) 0xc7, (byte) 0x73, (byte) 0x21, (byte) 0x8c,
                (byte) 0x7e, (byte) 0xc8, (byte) 0xee, (byte) 0x99
        };

        int count = 20;

        PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt, count, 128 * 8);
        Key key = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1").generateSecret(spec);
        return key;
    }

    private static String secretKeyToString(SecretKey secretKey) {
        return Base64.getEncoder().encodeToString(secretKey.getEncoded());
    }


    private static void writeFileCrypted(String text, String fileName) throws IOException {
        FileUtils.writeStringToFile(new File(ApplicationConfig.WALLET_PATH + File.separator + "identities-wallet"),
                text, StandardCharsets.UTF_8);
    }
}
