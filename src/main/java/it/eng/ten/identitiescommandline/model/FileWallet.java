package it.eng.ten.identitiescommandline.model;

public class FileWallet {

    private String address;
    private KeyPair keyPair;

    private String status;

    public FileWallet() {
    }

    public FileWallet(String address, KeyPair keyPair) {
        this.address = address;
        this.keyPair = keyPair;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public KeyPair getkeyPair() {
        return keyPair;
    }

    public void setkeyPair(KeyPair keyPair) {
        this.keyPair = keyPair;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "FileWallet{" +
                "address='" + address + '\'' +
                ", keyPair=" + keyPair +
                '}';
    }
}
