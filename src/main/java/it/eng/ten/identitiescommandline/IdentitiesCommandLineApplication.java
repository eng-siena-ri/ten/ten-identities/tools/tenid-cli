package it.eng.ten.identitiescommandline;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IdentitiesCommandLineApplication implements CommandLineRunner {

	@Autowired
	private IdentitiesProcessor identitiesProcessor;

	public static void main(String[] args) {
		SpringApplication.run(IdentitiesCommandLineApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
			identitiesProcessor.process(args);
	}
}
