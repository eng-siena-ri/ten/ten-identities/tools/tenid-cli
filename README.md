# tenid-cli

**tenid-cli** is an application able to manage a wallet in the context of ten-identies. The application is released as Jar executable.
When the first identity is generated, the application will create a file called "identities-wallet" where in subsequent uses it will add the other identities. The file is generated in the user-home of the user who launches the jar.


## Usage
```
java - jar <path to the executable file>_

- c <command: list, stat, gen , show> (mandatory)
- p <password: password of your wallet> (mandatory)
- a <address: address Key of identity that we want to display, mandatory together with the command SHOW> 
- n <name: name for address> (optional)
- h  <HELP: Commands list> (optional)
```

## Example

```
Generate a new identity:
> java -jar tenid-cli.jar -c gen -p password -n name

Show all identities in the wallet
> java -jar tenid-cli.jar -c list -p password

Show specific identity by address
> java -jar tenid-cli.jar -c show -p password  -a 5aBCvP1QoskN5pSxL1nSA221utj5LBAfu
```

## License <a name="license"></a>

tenid-cli Project source code files are made available under the Apache License, Version 2.0 (Apache-2.0), located in the [LICENSE](LICENSE) file.