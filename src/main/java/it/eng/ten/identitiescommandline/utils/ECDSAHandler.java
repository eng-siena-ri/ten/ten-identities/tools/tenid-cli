package it.eng.ten.identitiescommandline.utils;

import it.eng.ten.identitiescommandline.model.ECDSApublicKeyXY;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.math.ec.ECCurve;

import java.math.BigInteger;
import java.security.*;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.InvalidKeySpecException;

/**
 * @author Antonio Scatoloni on 10/09/2020
 **/
public class ECDSAHandler {
    public static ECPublicKey hexTOPublicKey(String jsonXY) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeySpecException, DecoderException {

        ECDSApublicKeyXY xy = null;
        try {
            xy = (ECDSApublicKeyXY) JsonHandler.convertFromJson(jsonXY, ECDSApublicKeyXY.class);

        } catch (Exception e) {
            e.printStackTrace();
        }
        final byte[] bytesX = Hex.decodeHex(xy.getX());
        final byte[] bytesy = Hex.decodeHex(xy.getY());
        BigInteger x = new BigInteger(bytesX);
        BigInteger y = new BigInteger(bytesy);
        java.security.spec.ECPoint w = new java.security.spec.ECPoint(x, y);
        ECNamedCurveParameterSpec params = ECNamedCurveTable.getParameterSpec("secp256r1");
        KeyFactory fact = KeyFactory.getInstance("EC", "BC");
        ECCurve curve = params.getCurve();
        java.security.spec.EllipticCurve ellipticCurve = EC5Util.convertCurve(curve, params.getSeed());
        java.security.spec.ECParameterSpec params2 = EC5Util.convertSpec(ellipticCurve, params);
        java.security.spec.ECPublicKeySpec keySpec = new java.security.spec.ECPublicKeySpec(w, params2);
        return (ECPublicKey) fact.generatePublic(keySpec);
    }

    public static ECDSApublicKeyXY getPublicKeyAsHex(PublicKey publicKey) throws Exception {
        ECPublicKey ecPublicKey = (ECPublicKey) publicKey;
        final ECParameterSpec params = ecPublicKey.getParams();
        ECPoint ecPoint = ecPublicKey.getW();
        byte[] affineXBytes = ecPoint.getAffineX().toByteArray();
        byte[] affineYBytes = ecPoint.getAffineY().toByteArray();
        String hexX = Hex.encodeHexString(affineXBytes);
        String hexY = Hex.encodeHexString(affineYBytes);
        ECDSApublicKeyXY ECDSApublicKeyXY = new ECDSApublicKeyXY(hexX, hexY);
//        ECDSApublicKeyXY ECDSApublicKeyXY = new ECDSApublicKeyXY(hexX, hexY, JsonHandler.convertToJson(params));
        return ECDSApublicKeyXY;
    }

    public static org.apache.commons.lang3.tuple.Pair<PrivateKey, PublicKey> generateKey() throws Exception {

        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("EC");
        keyGen.initialize(new ECGenParameterSpec("secp256r1"), new SecureRandom());

        KeyPair pair = keyGen.generateKeyPair();
        ECPrivateKey priv = (ECPrivateKey) pair.getPrivate();
//        ECPublicKey ecPublicKey = (ECPublicKey) pair.getPublic();
//        System.out.println(ecPublicKey.getParams().getCofactor());
//        System.out.println(ecPublicKey.getParams().getOrder().intValue());
//        System.out.println(ecPublicKey.getParams().getCurve());

        PublicKey pub = pair.getPublic();
        ImmutablePair<PrivateKey, PublicKey> pairs = new ImmutablePair<>(priv, pub);
//        writeKey(priv, paths, true);
//        writeKey(pub, paths, false);
        return pairs;
    }





    /*public static void main(String[] args) throws Exception {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());


        final Pair<PrivateKey, PublicKey> privateKeyPublicKeyPair = generateKey();
        Base64.Encoder encoder = Base64.getEncoder();

        String publicKeyStr = encoder.encodeToString(privateKeyPublicKeyPair.getRight().getEncoded());
        ECDSApublicKeyXY ecdsApublicKeyXY;

        System.out.println("Pub   " + publicKeyStr);
        ecdsApublicKeyXY = getPublicKeyAsHex(privateKeyPublicKeyPair.getRight());

        final byte[] bytesX = Hex.decodeHex(ecdsApublicKeyXY.getX());
        final byte[] bytesy = Hex.decodeHex(ecdsApublicKeyXY.getY());
        BigInteger x = new BigInteger(bytesX);
        BigInteger y = new BigInteger(bytesy);

//        MEECAQAwEwYHKoZIzj0CAQYIKoZIzj0DAQcEJzAlAgEBBCBI5v+SFrf/+A4VLS2ExvOs5L0jbqzw79uCpq0gwEv/eQ==
//        BigInteger x = new BigInteger("055ba2e3e70d2e88b64aaf23ac0889f5aa1ff851ba632f610f4d45c5764fd7e1");
//        BigInteger y = new BigInteger("082dbda6020eed9e51cd2355ae9ac31328c708bdae5ee4ec65ba97733c8a8abd");
        try {
            final ECPublicKey publicKey = hexTOPublicKey(x, y);
            PublicKey publicKey1 = publicKey;
            String publicKeyStr1 = encoder.encodeToString(publicKey1.getEncoded());
            System.out.println(publicKeyStr1);
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }*/
}