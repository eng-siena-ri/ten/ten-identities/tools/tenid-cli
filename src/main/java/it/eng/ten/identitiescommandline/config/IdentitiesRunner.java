package it.eng.ten.identitiescommandline.config;

import com.beust.jcommander.Parameter;
import lombok.Data;

@Data
public class IdentitiesRunner {


    @Parameter(names = {"-c", "--command",}, help = true, required = true, description = "gen , list , show , stat")
    private String command;

    @Parameter(names = {"-p", "--password",}, help = true, required = true, description = "Password for personal " +
            "wallet")
    private String password;

    @Parameter(names = {"-a", "--address",}, help = true, description = "Identity address")
    private String address;

    @Parameter(names = {"-n", "--name",}, help = true, description = "Name for address")
    private String name;

    @Parameter(names = {"-h", "--help",}, help = true, description = "Help")
    private boolean help;
    @Parameter(names = {"old"}, help = true, description = "old")
    private boolean old;
}
