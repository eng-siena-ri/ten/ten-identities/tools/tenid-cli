package it.eng.ten.identitiescommandline.utils;

import it.eng.ten.identitiescommandline.config.ApplicationConfig;
import it.eng.ten.identitiescommandline.model.FileWallet;
import it.eng.ten.identitiescommandline.utils.crypto.ChiperUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.BadPaddingException;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;

public class WalletManager {

    private static final Logger logger = LogManager.getLogger(WalletManager.class);

    public static void generate(String psw, String name) throws Exception {
        if (!name.equals(ApplicationConfig.INIT_NAME)) {
            final Collection<FileWallet> fileWallets = openWallet(psw);
            for (FileWallet fileWallet : fileWallets) {
                if (fileWallet.getkeyPair().getName().equals(name)) {
                    throw new Exception("An identity with the same already exists, please retry");
                }
            }
            ChiperHandler.createWallet(psw, name);
        } else
            ChiperHandler.createWallet(psw, name);
    }

    public static void list(String psw) {
        try {
            final Collection<FileWallet> fileWallets = openWallet(psw);
            if (fileWallets.size() < 2)
                System.out.println("iWallet empty please create identity!");
            for (FileWallet fileWallet : fileWallets
            ) {
                if (fileWallet.getkeyPair().getName().equals(ApplicationConfig.INIT_NAME)) continue;
                System.out.println("\n Address: " + fileWallet.getAddress() + "\n Name: " + fileWallet.getkeyPair().getName() + "\n PubKey: " + fileWallet.getkeyPair().getPublicKey());
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }


    }

    public static void show(String psw, String addr) {
        try {
            final Collection<FileWallet> fileWallets = openWallet(psw);
            for (FileWallet fileWallet : fileWallets
            ) {
                if (addr.equals(fileWallet.getAddress())) {
                    System.out.println("\n Address: " + fileWallet.getAddress() + "\n Public Key: " + fileWallet.getkeyPair().getPublicKey() + "\n Key type: " + fileWallet.getkeyPair().getKeyType() + "\n Name: " + fileWallet.getkeyPair().getName());
                    return;
                }
            }
            System.out.println("Identity not found with address: " + addr);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }

    public static void stat(String psw) {
        try {
            final Collection<FileWallet> fileWallets = openWallet(psw);
            System.out.println("Identity presents in your wallet are: " + (fileWallets.size() - 1));
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

    }

    private static Collection<FileWallet> openWallet(String psw) throws Exception {
        Path path = Paths.get(ApplicationConfig.WALLET_PATH + File.separator + "identities-wallet");
        //logger.info("Read wallet in path : " + ApplicationConfig.WALLET_PATH + File.separator + "identities-wallet");
        try {
            if (Files.exists(path)) {
                String walletString = new String(Files.readAllBytes(path));
                final String decrypt = ChiperUtils.decrypt(psw, walletString);
                Collection<FileWallet> fileWallets = (Collection<FileWallet>) JsonHandler.convertFromJson(decrypt,
                        FileWallet.class, true);
                return fileWallets;
            } else {
                throw new Exception("Wallet not found!");
            }
        } catch (BadPaddingException e) {
            throw new Exception("The chosen password is not valid, please retry");
        }
    }

    public static void verifyWalletPath() {
        String envName = "WALLET_PATH";
        try {
            final String getenv = System.getenv(envName);
            if (getenv == null || getenv.isEmpty()) {
                Path parentDir = //PATH personal node
                        Paths.get(Paths.get(System.getProperty("user.dir")).getParent() + File.separator + "conf" + File.separator + "wallet");
                if (Files.exists(parentDir))
                    ApplicationConfig.WALLET_PATH = parentDir.toString();
                else
                    ApplicationConfig.WALLET_PATH = System.getProperty("user.home");
                // System.out.println("Environment Variable not found!\nThe wallet file at: " + ApplicationConfig
                // .WALLET_PATH);
            } else {
                ApplicationConfig.WALLET_PATH = getenv;
               // System.out.println("Environment Variable not found!\nThe wallet file at: " + ApplicationConfig
               //  .WALLET_PATH);
            }
        } catch (Exception ignored) {
        }
    }
}
