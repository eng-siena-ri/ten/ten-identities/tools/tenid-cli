package it.eng.ten.identitiescommandline;

import com.beust.jcommander.JCommander;
import it.eng.ten.identitiescommandline.config.ApplicationConfig;
import it.eng.ten.identitiescommandline.config.IdentitiesRunner;
import it.eng.ten.identitiescommandline.utils.ChiperHandler;
import it.eng.ten.identitiescommandline.utils.Colors;
import it.eng.ten.identitiescommandline.utils.WalletManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

import static it.eng.ten.identitiescommandline.utils.Colors.*;

@Component
public class IdentitiesProcessor {
    private static final Logger logger = LogManager.getLogger(IdentitiesProcessor.class);

    @Autowired
    private ApplicationConfig applicationConfig;

    private IdentitiesRunner identitiesRunner;
    private static boolean initWallet; // reset | init Wallet

    public void process(String... args) {
        try {
            System.out.println("" +
                    " ___   _______  _______  _______ ____     \n" +
                    "|   | |       ||       ||       ||   |    \n" +
                    "|   | |_     _||   _   ||   _   ||   |    \n" +
                    "|   |   |   |  |  | |  ||  | |  ||   |    \n" +
                    "|   |   |   |  |  |_|  ||  |_|  ||   |___ \n" +
                    "|   |   |   |  |       ||       ||       |\n" +
                    "|___|   |___|  |_______||_______||_______|");
            final JCommander jCommander = applicationConfig.getJCommander();
            //jCommander.parse(args);
            identitiesRunner = applicationConfig.getRunner();
            if (identitiesRunner.isHelp()) {
                jCommander.usage();
                System.out.print("Examples:\n" + "Generate an identity using these options: java -jar " +
                        "./target/ten-identities-command-line.jar" + " -c gen -p <wallet_password> -n " +
                        "<choose_a_name>\n" + "List all identities stored in a wallet using these options: java -jar "
                        + "./target/ten-identities-command-line.jar -c list -p <wallet_password>\n");
            } else if (identitiesRunner.isOld()) {
                executeOperation(identitiesRunner.getCommand());
            } else {
                menu(true);
            }
        } catch (Exception e) {
            logger.error("Error encountered in process with message: " + e.getMessage());
        }
    }

    private static void menu(boolean init) {
        if (init) {
            WalletManager.verifyWalletPath();
            initWallet = !ChiperHandler.existsIdentityFile();
        }
        switch (menuData()) {
            case 1:
                System.out.println("WARNING! If you proceed further, the existing iWallet will be lost forever!");
                System.out.println("WARNING! Keep your password safe, the iWallet cannot be recovered without it!");
                String passwordInput = passwordInput(true);
                try {
                    WalletManager.generate(passwordInput, ApplicationConfig.INIT_NAME);
                    System.out.println("iWallet initialized and ready for use!");
                    initWallet = false;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    initWallet = true;
                }
                menu(false);
                break;
            case 2:
                passwordInput = passwordInput(false);
                WalletManager.list(passwordInput);
                menu(false);
                break;
            case 3:
                passwordInput = passwordInput(false);
                String nameInput = nameInput();
                try {
                    WalletManager.generate(passwordInput, nameInput);
                    System.out.println("Identity " + nameInput + " created!!");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                menu(false);
                break;
            case 4:
                System.out.println("Bye Bye!");
                System.exit(0);
                break;
            case 5:
                System.out.println("Option 4 selected");
                menu(false);
                break;
            default:
                System.out.println("Option not valid!");
                break;
        }
    }

    private static String passwordInput(boolean isInitWallet) {
        if (isInitWallet)
            System.out.println("Set your iWallet password (no whitespace, case-sensitive, max length 25):");
        else
            System.out.println("iWallet password:");
        String password = detectEnterKey();
        if (password == null || password.isEmpty() || password.trim().isEmpty() || password.length() > 25) {
            System.out.println("The chosen password is not valid, please retry");
            passwordInput(isInitWallet);
        }
        return password.trim();
    }

    private static String nameInput() {
        System.out.println("Assign a name to this identity (alphanumeric, case-sensitive, max length 25):");
        String name = detectEnterKey();
        if (name == null || name.isEmpty() || name.trim().isEmpty() || name.length() > 25) {
            System.out.println("The chosen name is not valid, please retry ");
            passwordInput(false);
        }
        return name.trim();
    }

    private static String detectEnterKey() {
        //System.out.println("" + ANSI_YELLOW);
        Scanner sc = new Scanner(System.in);
        String readString = sc.nextLine();
        //System.out.println("" + ANSI_RESET);
        if (readString.trim().isEmpty()) {
            System.out.println("Enter key pressed...Back to menu!");
            menu(false);
        } else
            return readString;
        return null;
    }

    private void executeOperation(String command) {
        WalletManager.verifyWalletPath();
        try {
            switch (command) {
                case "list":
                    WalletManager.list(identitiesRunner.getPassword());
                    break;
                case "stat":
                    WalletManager.stat(identitiesRunner.getPassword());
                    break;
                case "gen":
                    WalletManager.generate(identitiesRunner.getPassword(), identitiesRunner.getName());
                    break;
                case "show":
                    WalletManager.show(identitiesRunner.getPassword(), identitiesRunner.getAddress());
                    break;
            }
        } catch (Exception e) {
            logger.error("Error encountered in process with message: " + e.getMessage());
        }
    }

    public static int menuData() {
        int selection;
        Scanner sc = new Scanner(System.in);
        System.out.println("Select action:");
        System.out.println("-------------------");
        if (initWallet)
            System.out.println("1 - Initialize iWallet ");
        else System.out.println("1 - Reset iWallet ");
        System.out.println("2 - List identities ");
        System.out.println("3 - Create identity ");
        System.out.println("4 - Exit ");
        selection = sc.nextInt();
        return selection;
    }

}